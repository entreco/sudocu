# Sudocu

Solve Sudoku's in kotlin


# Credits

This is an adapted version from [Peter Norvig](http://norvig.com/sudoku.html)'s Sudoku solver.
See his website for a detailed explanation of the algorithm used.

# Kotlin Multiplatform
1. Add `MavenCentral` to `root/build.gradle`

``` groovy
repositories {
    mavenCentral()
}
```

2. Add `sudoco-solver` to `app/build.gradle`
``` groovy
dependencies{
    implementation "nl.entreco.sudoco:sudoco-solver:1.0.0"
}
```

3. Create Sudoku
``` kotlin
    val sudoku = Sudoco.Builder()
        .withEntries(listOf( /* comma seperated integers e.g. 0,0,2,0,0,3,4,0 */))
        .build()
```
4. Solve the Sudoku
``` kotlin
    Solver(sudoku, algorithm=TreeSearching()).solve()
```
