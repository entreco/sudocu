## KEY INFO:

>Real name: Entreco
>Email address: info@entreco.nl
>You selected this USER-ID:
>    "Entreco <info@entreco.nl>"
>
>Change (N)ame, (E)mail, or (O)kay/(Q)uit? O
>We need to generate a lot of random bytes. It is a good idea to perform
>some other action (type on the keyboard, move the mouse, utilize the
>disks) during the prime generation; this gives the random number
>generator a better chance to gain enough entropy.
>We need to generate a lot of random bytes. It is a good idea to perform
>some other action (type on the keyboard, move the mouse, utilize the
>disks) during the prime generation; this gives the random number
>generator a better chance to gain enough entropy.
>gpg: key C27E47991E95F2E5 marked as ultimately trusted
>gpg: revocation certificate stored as '/Users/entreco/.gnupg/openpgp-revocs.d/E818DA4F0B7491BA45958004C27E47991E95F2E5.rev'
>public and secret key created and signed.
>
>pub   rsa2048 2020-03-29 [SC] [expires: 2022-03-29]
>      E818DA4F0B7491BA45958004C27E47991E95F2E5
>uid                      Entreco <info@entreco.nl>
>sub   rsa2048 2020-03-29 [E] [expires: 2022-03-29]

## KEY INFO EXTRA:
1. Create key using GPG Tool or Command line
1. export secret key to gpg:
`gpg --export-secret-keys >~/.gnupg/secring.gpg`
1. Use in `~/.gradle/gradle.properties`