package nl.entreco.sudoco.shared.printers

import nl.entreco.sudoco.shared.ArrayExtensions.rows
import nl.entreco.sudoco.shared.Printer
import nl.entreco.sudoco.shared.Solver
import nl.entreco.sudoco.shared.Sudoco

/**
 * Prints a Sudocu board with some asci formatting
```
    ╔═══╤═══╤═══╦═══╤═══╤═══╦═══╤═══╤═══╗
    ║ 8 │ 5 │   ║   │   │ 2 ║ 4 │   │   ║
    ╟───┼───┼───╫───┼───┼───╫───┼───┼───╢
    ║ 7 │ 2 │   ║   │   │   ║   │   │ 9 ║
    ╟───┼───┼───╫───┼───┼───╫───┼───┼───╢
    ║   │   │ 4 ║   │   │   ║   │   │   ║
    ╠═══╪═══╪═══╬═══╪═══╪═══╬═══╪═══╪═══╣
    ║   │   │   ║ 1 │   │ 7 ║   │   │ 2 ║
    ╟───┼───┼───╫───┼───┼───╫───┼───┼───╢
    ║ 3 │   │ 5 ║   │   │   ║ 9 │   │   ║
    ╟───┼───┼───╫───┼───┼───╫───┼───┼───╢
    ║   │ 4 │   ║   │   │   ║   │   │   ║
    ╠═══╪═══╪═══╬═══╪═══╪═══╬═══╪═══╪═══╣
    ║   │   │   ║   │ 8 │   ║   │ 7 │   ║
    ╟───┼───┼───╫───┼───┼───╫───┼───┼───╢
    ║   │ 1 │ 7 ║   │   │   ║   │   │   ║
    ╟───┼───┼───╫───┼───┼───╫───┼───┼───╢
    ║   │   │   ║   │ 3 │ 6 ║   │ 4 │   ║
    ╚═══╧═══╧═══╩═══╧═══╧═══╩═══╧═══╧═══╝
 ```
 */
class AsciiPrinter : Printer {

    companion object {
        private const val thick = "═══"
        private const val thin = "───"
    }

    override fun solution(status: Solver.Status, duration: String, board: Sudoco) {
        println("========================================================================")
        println(" ")
        println("Done $status  ($duration)")
        println(" ")
        println(print(board))
        println("========================================================================")
    }

    override fun print(sudoco: Sudoco): String {
        return print(sudoco.data.rows())
    }

    private fun print(data: Array<IntArray>): String {
        if (data.isEmpty()) return ""

        val size = data.size
        return data.mapIndexed { index, rows ->
            when {
                index == 0 -> firstRow(size) + "\n" + values(rows, size)
                index % 3 == 0 -> thickRow(size) + "\n" + values(rows, size)
                else -> thinRow(size) + "\n" + values(rows, size)
            }
        }.joinToString("\n", postfix = "\n" + lastRow(size)) { it }
    }

    private fun firstRow(size: Int): String = (0..size).mapIndexed { index, _ ->
        when {
            index == 0 -> "╔$thick"
            index == size -> "╗"
            index % 3 == 0 -> "╦$thick"
            else -> "╤$thick"
        }
    }.joinToString("") { it }


    private fun lastRow(size: Int): String = (0..size).mapIndexed { index, _ ->
        when {
            index == 0 -> "╚$thick"
            index == size -> "╝"
            index % 3 == 0 -> "╩$thick"
            else -> "╧$thick"
        }
    }.joinToString("") { it }

    private fun thickRow(size: Int): String = (0..size).mapIndexed { index, _ ->
        when {
            index == 0 -> "╠$thick"
            index == size -> "╣"
            index % 3 == 0 -> "╬$thick"
            else -> "╪$thick"
        }
    }.joinToString("") { it }

    private fun thinRow(size: Int): String = (0..size).mapIndexed { index, _ ->
        when {
            index == 0 -> "╟$thin"
            index == size -> "╢"
            index % 3 == 0 -> "╫$thin"
            else -> "┼$thin"
        }
    }.joinToString("") { it }

    private fun values(row: IntArray, size: Int): String = (0..size).mapIndexed { index, _ ->
        when {
            index == 0 -> "║${value(row[index])}"
            index == size -> "║"
            index % 3 == 0 -> "║${value(row[index])}"
            else -> "│${value(row[index])}"
        }
    }.joinToString("") { it }

    private fun value(value: Int): String {
        val aha = if (value == 0) " " else value
        return " $aha "
    }
}