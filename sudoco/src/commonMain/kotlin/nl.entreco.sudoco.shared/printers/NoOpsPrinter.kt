package nl.entreco.sudoco.shared.printers

import nl.entreco.sudoco.shared.Printer
import nl.entreco.sudoco.shared.Solver
import nl.entreco.sudoco.shared.Sudoco

/**
 * Prints empty string,
 * usefull for measuring performance,
 * to exclude printing
 */
class NoOpsPrinter : Printer {
    override fun print(sudoco: Sudoco) = ""
    override fun solution(status: Solver.Status, duration: String, board: Sudoco) = Unit
}