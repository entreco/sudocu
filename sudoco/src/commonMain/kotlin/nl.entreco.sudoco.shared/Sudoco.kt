package nl.entreco.sudoco.shared

import nl.entreco.sudoco.shared.ArrayExtensions.cols
import nl.entreco.sudoco.shared.ArrayExtensions.nonets
import nl.entreco.sudoco.shared.ArrayExtensions.rows

/**
 * Sudoco.
 *
 * 1. Create a Sudocu using the Builder:
``` kotlin
   val sudo = Sudoco.Builder()
        .withEntries(listOf(0,0,0,8,9,3,6))
        .build()
```
 *
 * 2. Solve using a Solver:
``` kotlin
    Solver(sudoco= sudo, algorithm= TreeSearching())
        .solve()
```
 */
class Sudoco private constructor(
    val size: Int,
    val data: IntArray
) {
    companion object {
        private const val BLANK = 0
        private const val DEFAULT_SIZE = 9
    }

    fun query(row: Int, col: Int) = data[row * size + col]
    fun blanks() = data.count { it == BLANK }
    fun isValid() = data.rows().all { validate(it) }
            && data.cols().all { validate(it) }
            && data.nonets().all { validate(it) }

    private fun validate(entries: IntArray): Boolean {
        val set = mutableSetOf<Int>()
        var blankCounter = -1
        entries.asSequence().forEachIndexed { index, number ->
            if (number == BLANK) {
                blankCounter++
            } else {
                set.add(number)
            }

            if (blankCounter + set.size != index) return false
        }
        return true
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || this::class != other::class) return false

        other as Sudoco

        if (size != other.size) return false
        if (!data.contentEquals(other.data)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = size
        result = 31 * result + data.contentHashCode()
        return result
    }

    /**
     * Builds a new Sudoku.
     * - .withValues to create an empty Sudoku with a single value set
     * - .withEntries to specify a complete sudoku at once
     */
    class Builder(private val size: Int = DEFAULT_SIZE) {

        private var _entries = IntArray(size * size)

        fun withValue(row: Int, col: Int, value: Int) = apply {
            _entries[row * size + col] = value
        }

        fun withEntries(entries: List<Int>) = apply {
            _entries = entries.toIntArray()
        }

        fun build(): Sudoco {
            require(_entries.size == size * size) { "invalid dimensions " }
            val data = IntArray(size * size)
            _entries.forEachIndexed { index, i ->
                val row = index / size
                val col = index % size
                data[row * size + col] = i
            }

            return Sudoco(size, data)
        }
    }
}