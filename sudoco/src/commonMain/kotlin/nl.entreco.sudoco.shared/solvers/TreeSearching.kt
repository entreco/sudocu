package nl.entreco.sudoco.shared.solvers

import nl.entreco.sudoco.shared.*

/**
 * Algorithm adapted from Peter Norvig
 * http://norvig.com/sudoku.html
 */
class TreeSearching : Algo {

    private val digits = "123456789"
    private val rows = "ABCDEFGHI"
    private val cols = digits
    internal val squares = cross(rows, cols)
    internal val unitList = listOf(
        cols.map { c -> cross(rows, c.toString()) },
        rows.map { r -> cross(r.toString(), cols) },
        listOf("ABC", "DEF", "GHI").flatMap { rs ->
            listOf("123", "456", "789").map { cs ->
                cross(rs, cs)
            }
        }
    ).flatten()

    internal val units: Map<String, List<List<String>>> = squares.map { s ->
        s to unitList.filter { u -> u.contains(s) }
    }.toMap()

    internal val peers: Map<String, Set<String>> = squares.map { s ->
        s to units[s]!!.flatMap { it.minus(s) }.toSet()
    }.toMap()

    private fun cross(a: String, b: String): List<String> {
        val units = mutableListOf<String>()
        a.forEach { _a ->
            b.forEach { _b ->
                units.add("$_a$_b")
            }
        }
        return units
    }

    private var solution: List<Int> = emptyList()


    override fun solve(input: Sudoco): Sudoco {
        if (solution.isEmpty()) {
            solution = search(parse(input))?.map {
                it.value.toInt()
            } ?: emptyList()
        }

        val index = input.data.indexOfFirst { it == 0 }
        input.data[index] = solution[index]
        return input
    }

    private fun search(values: MutableMap<String, String>?): MutableMap<String, String>? {
        // "Using depth-first search and propagation, try all possible values."
        if (values == null) return null
        if (squares.all { s -> values[s]!!.length == 1 }) return values // ## Solved!

        // ## Chose the unfilled square s with the fewest possibilities
        // min( (len(values[s]), s)  )
        val min = squares.filter { s -> values[s]!!.length > 1 }.minBy { values[it]!!.length }!!
        return values[min]!!.map { d ->
            search(assign(values.toMutableMap(), min, SingleChar(d)))
        }.firstOrNull {
            it != null
        }
    }

    private fun parse(grid: Sudoco): MutableMap<String, String> {
        val values = squares.map { s -> s to digits }.toMap().toMutableMap()
        toValues(grid).forEach { entry ->
            val s = entry.key
            val d = entry.value[0]
            if (digits.contains(d) && assign(
                    values,
                    s,
                    SingleChar(d)
                ) == null
            ) { // Fail if we can't assign d to square s
                throw IllegalStateException("Fail")
            }
        }
        return values
    }

    private fun toValues(grid: Sudoco): Map<String, String> {
        return squares.mapIndexed { index, s ->
            val value = grid.data[index]
            s to if (digits.contains(value.toString())) value.toString() else "."
        }.toMap()
    }

    private fun assign(values: MutableMap<String, String>, s: String, d: SingleChar): MutableMap<String, String>? {
        val others = values.get(s)!!.replace(d.value(), "")
        val allOthers = others.map { d2 -> eliminate(values, s, SingleChar(d2)) }
        return if (allOthers.all { it != null }) return values
        else null
    }

    private fun eliminate(values: MutableMap<String, String>, s: String, d: SingleChar): MutableMap<String, String>? {
        if (!values.get(s)!!.contains(d.value())) {
            return values
        }

        values[s] = values[s]!!.replace(d.value(), "")

        // (1) Eliminate from Peers
        when (values[s]!!.length) {
            0 -> {  // Contradiction removed last value
                return null
            }
            1 -> {
                val d2 = SingleChar(values[s]!![0])
                val notAll = peers[s]!!.map { s2 -> eliminate(values, s2, d2) }
                if (!notAll.all { it != null }) {
                    return null
                }
            }
        }

        // (2) Use value if 1 left
        units[s]!!.map { u ->
            val dplaces: List<String> = u.filter { values[it]!!.contains(d.value()) }
            when (dplaces.size) {
                0 -> {  // Contradiction: no place for value
                    return null
                }
                1 -> { // Only 1 place left -> assign it there
                    val assign = assign(values, dplaces[0], d)
                    if (assign == null) {
                        return null
                    }
                }
            }
        }

        return values
    }
}

data class SingleChar(val char: Char) {
    fun value() = char.toString()
}