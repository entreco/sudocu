package nl.entreco.sudoco.shared.solvers

import nl.entreco.sudoco.shared.*
import nl.entreco.sudoco.shared.ArrayExtensions.cols
import nl.entreco.sudoco.shared.ArrayExtensions.nonet
import nl.entreco.sudoco.shared.ArrayExtensions.powerSum
import nl.entreco.sudoco.shared.ArrayExtensions.rows

class SimpleAlgo : Algo {
    private val desiredSet = setOf(1, 2, 3, 4, 5, 6, 7, 8, 9)
    override fun solve(input: Sudoco): Sudoco {
        val rows = input.data.rows()
        val cols = input.data.cols()

        val memory = IntArray(input.data.size)

        rows.forEachIndexed { r, row ->
            cols.forEachIndexed { c, col ->
                val current = input.query(r, c)
                if (current == 0) {
                    // Any combination of Row+Col has exactly 1 NONET at the Intersection.
                    // We need to grab the NONET at that intersection
                    val nonet = input.data.nonet(r, c)
                    val combined = intArrayOf(*row, *col, *nonet)
                    val set = combined.filter { it != 0 }.toSet()
                    val wow = desiredSet - set
                    memory[r * input.size + c] = wow.powerSum()

                    if (wow.size == 1) {
                        input.data[r * input.size + c] = wow.first()
                        return input
                    }
                } else {
                    memory[r * input.size + c] = current
                }
            }
        }

        // If we reach this point -> we have to switch tactics
        val memRows = memory.rows()
        val memCols = memory.cols()

        memRows.forEachIndexed { r, row ->
            memCols.forEachIndexed { c, col ->
                val current = input.query(r, c)
                if (current == 0) {
                    // Any combination of Row+Col has exactly 1 NONET at the Intersection.
                    // We need to grab the NONET at that intersection
                    val nonet = memory.nonet(r, c)
                    val combined = intArrayOf(*row, *col, *nonet)
                    val set = combined.filter { it != 0 }.toSet()
                    val wow = desiredSet - set

                    if (wow.size == 2) {
                        val rest = set.filter { !desiredSet.contains(it) }.map { Pair(it / 10, it - 10) }
                        val anyFirst = wow.firstOrNull { w -> rest.all { it.first == w } }
                        val anySecond = wow.firstOrNull { w -> rest.all { it.second == w } }
                        if (anyFirst != null) input.data[r * input.size + c] = wow.first()
                        if (anySecond != null) input.data[r * input.size + c] = wow.last()
                        return input
                    }
                }
            }
        }



        return input
    }
}