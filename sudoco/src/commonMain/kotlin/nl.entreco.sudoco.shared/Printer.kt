package nl.entreco.sudoco.shared

interface Printer {
    /**
     * Print a Sudoku to the console
     * @param sudoco
     */
    fun print(sudoco: Sudoco) : String

    /**
     * Print the solution of solving a Sudoku
     */
    fun solution(status: Solver.Status, duration: String, board: Sudoco)
}