package nl.entreco.sudoco.shared

import kotlin.math.floor
import kotlin.math.pow
import kotlin.math.sqrt

internal object ArrayExtensions {

    fun IntArray.rows(): Array<IntArray> {
        val size = sqrt(this.size.toFloat()).toInt()
        val squareData = Array(size) {
            IntArray(size)
        }
        this.forEachIndexed { index, i ->
            val row = index / size
            val col = index % size
            squareData[row][col] = i
        }

        return squareData
    }

    fun IntArray.cols(): Array<IntArray> {
        val size = sqrt(this.size.toFloat()).toInt()
        val squareData = Array(size) {
            IntArray(size)
        }
        this.forEachIndexed { index, i ->
            val row = index % size
            val col = index / size
            squareData[row][col] = i
        }

        return squareData
    }


    fun IntArray.nonets(): Array<IntArray> {
        // TODO: Make this fully dynamic
        if (this.size != 81) return arrayOf(rows().flatMap { it.asIterable() }.toIntArray())

        val width = sqrt(this.size.toFloat())
        val block = floor(sqrt(width)).toInt()
        val grouped = mutableMapOf<Int, IntArray>()
        this.forEachIndexed { index, i ->
            val row = ((index / size) / block)
            val col = ((index % size) / block)
            val prev = grouped[col]
            grouped[col] = prev?.plus(i) ?: intArrayOf(i)
        }
        val block1 = intArrayOf(*grouped[0]!!, *grouped[3]!!, *grouped[6]!!)
        val block2 = intArrayOf(*grouped[1]!!, *grouped[4]!!, *grouped[7]!!)
        val block3 = intArrayOf(*grouped[2]!!, *grouped[5]!!, *grouped[8]!!)
        val block4 = intArrayOf(*grouped[9]!!, *grouped[12]!!, *grouped[15]!!)
        val block5 = intArrayOf(*grouped[10]!!, *grouped[13]!!, *grouped[16]!!)
        val block6 = intArrayOf(*grouped[11]!!, *grouped[14]!!, *grouped[17]!!)
        val block7 = intArrayOf(*grouped[18]!!, *grouped[21]!!, *grouped[24]!!)
        val block8 = intArrayOf(*grouped[19]!!, *grouped[22]!!, *grouped[25]!!)
        val block9 = intArrayOf(*grouped[20]!!, *grouped[23]!!, *grouped[26]!!)

        return arrayOf(block1, block2, block3, block4, block5, block6, block7, block8, block9)
    }

    fun IntArray.nonet(row: Int, col: Int): IntArray {
        return nonets()[nonetIndex(row, col)]
    }

    internal fun nonetIndex(row: Int, col: Int): Int {
        return (row / 3) * 3 + col / 3
    }

    fun Collection<Int>.powerSum(): Int {
        if (size <= 0) return 0
        else if (size == 1) return first()
        else if (size > 2) return 0

        val factor = size - 1
        return mapIndexed { index, i -> i * 10f.pow(factor - index) }.sum().toInt()
    }
}
