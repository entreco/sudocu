package nl.entreco.sudoco.shared

import nl.entreco.sudoco.shared.printers.NoOpsPrinter
import nl.entreco.sudoco.shared.solvers.TreeSearching
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

/**
 * Solves a sudoku step-by-step
 * @param sudoco - the one to solve
 * @param algorithm - which algorithm to use
 * @param printer - how to print the Sudoku
 */
class Solver(
    private val sudoco: Sudoco,
    private val algorithm: Algo = TreeSearching(),
    private val printer: Printer = NoOpsPrinter()
) {
    @ExperimentalTime
    fun solve() {
        require(sudoco.isValid()) { "This sudoco seems to be invalid! sorry" }

        val board = sudoco
        var status = if (board.blanks() > 0) Status.Iterating else Status.Solved

        val duration = measureTime {

            while (status is Status.Iterating) {
                val numberOfBlanks = board.blanks()
                when (algorithm.solve(board).blanks()) {
                    0 -> status = Status.Solved
                    numberOfBlanks -> status = Status.Stuck
                }
            }
        }

        printer.solution(status, duration.toString(), board)
    }

    sealed class Status {
        object Solved : Status()
        object Stuck : Status()
        object Iterating : Status()
    }
}

interface Algo {
    fun solve(input: Sudoco): Sudoco
}

