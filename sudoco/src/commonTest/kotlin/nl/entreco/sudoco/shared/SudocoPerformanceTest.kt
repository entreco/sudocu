package nl.entreco.sudoco.shared

import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.time.ExperimentalTime

class SudocoPerformanceTest {

    companion object{
        private const val NUMBER_RUNS = 100000
    }

    @Test
    fun `should not allow rows with duplicates (exactly 9) 1`() {
        (0..NUMBER_RUNS).forEach {
            val entries = listOf(1, 1, 3, 4, 5, 6, 7, 8, 9).shuffled()
            assertFalse(row(entries))
        }
    }

    @Test
    fun `should not allow rows with duplicates (exactly 9) 2a`() {
        (0..NUMBER_RUNS).forEach {
            val entries = listOf(1, 1, 3, 4, 5, 6, 7, 8, 9).shuffled()
            assertFalse(row2(entries))
        }
    }

    @Test
    fun `should not allow rows with duplicates (exactly 9) 2b worst case`() {
        (0..NUMBER_RUNS).forEach {
            val entries = listOf(1, 2, 3, 4, 5, 6, 7, 9, 9)
            assertFalse(row2(entries))
        }
    }

    @Test
    fun `should not allow rows with duplicates (exactly 9) 3 `() {
        (0..NUMBER_RUNS).forEach {
            val entries = listOf(1, 2, 3, 4, 5, 6, 7, 9, 9).shuffled()
            assertFalse(row3(entries))
        }
    }

    // 1) 188 223 204 179 212 169
    // 2) 45 84 51 45 55 46
    private fun row(entries: List<Int>): Boolean {
        val expected = 1..9
        return entries.sorted() == expected.toList()
    }

    private fun row2(entries: List<Int>): Boolean {
        val set = mutableSetOf<Int>()
        entries.asSequence().forEachIndexed { index, i ->
            set.add(i)
            if(set.size != index) return false
        }
        return true
    }

    private fun row3(entries: List<Int>): Boolean {
        val set = mutableSetOf<Int>()
        var blankCounter = -1
        entries.asSequence().forEachIndexed { index, number ->
            if (number == 0) {
                blankCounter++
            } else {
                set.add(number)
            }

            if (blankCounter + set.size != index) return false
        }
        return true
    }
}