package nl.entreco.sudoco.shared

import nl.entreco.sudoco.shared.solvers.SimpleAlgo
import nl.entreco.sudoco.shared.solvers.TreeSearching
import kotlin.test.Ignore
import kotlin.test.Test
import kotlin.test.assertTrue
import kotlin.time.ExperimentalTime

@ExperimentalTime
class SolverPerformanceExpertTest {

    companion object{
        private const val NUMBER_RUNS = 1000
    }

    private val expert = Sudoco.Builder().withEntries(listOf(0,9,0,0,0,0,3,0,8,0,0,3,5,9,0,6,0,0,0,0,0,0,0,0,7,0,0,0,0,2,8,0,0,0,0,5,0,1,0,3,0,0,0,0,0,4,0,0,0,0,6,0,8,1,5,0,0,0,2,7,0,0,0,0,0,9,0,0,0,0,0,0,0,0,0,0,1,0,0,6,0)).build()

    @Test
    fun `should efficiently solve hard sudoku - searching`() {
        (0..NUMBER_RUNS).forEach {
            Solver(
                expert,
                TreeSearching()
            ).solve()
            assertTrue(expert.blanks() == 0 && expert.isValid())
        }
    }

}