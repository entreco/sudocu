package nl.entreco.sudoco.shared

import nl.entreco.sudoco.shared.solvers.SimpleAlgo
import nl.entreco.sudoco.shared.solvers.TreeSearching
import kotlin.test.Test
import kotlin.test.assertTrue
import kotlin.time.ExperimentalTime

@ExperimentalTime
class SolverPerformanceHardTest {

    companion object{
        private const val NUMBER_RUNS = 1000
    }

    private val hard = Sudoco.Builder().withEntries(listOf(4,0,0,0,0,8,0,0,2,0,6,0,0,2,0,8,0,5,0,0,0,0,6,0,0,3,0,3,9,6,0,0,0,1,2,8,0,0,7,0,0,2,0,4,0,2,0,4,0,0,0,5,7,0,0,7,2,0,8,4,0,0,0,0,3,0,0,0,0,0,0,9,5,4,9,6,1,0,0,8,0)).build()

    @Test
    fun `should efficiently solve hard sudoku - simple`() {
        (0..NUMBER_RUNS).forEach {
            Solver(
                hard,
                SimpleAlgo()
            ).solve()
            assertTrue(hard.blanks() == 0 && hard.isValid())
        }
    }

    @Test
    fun `should efficiently solve hard sudoku - searching`() {
        (0..NUMBER_RUNS).forEach {
            Solver(
                hard,
                TreeSearching()
            ).solve()
            assertTrue(hard.blanks() == 0 && hard.isValid())
        }
    }

}