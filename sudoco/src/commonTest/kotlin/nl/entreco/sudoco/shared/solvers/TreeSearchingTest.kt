package nl.entreco.sudoco.shared.solvers

import nl.entreco.sudoco.shared.Algo
import nl.entreco.sudoco.shared.Solver
import nl.entreco.sudoco.shared.Sudoco
import nl.entreco.sudoco.shared.printers.AsciiPrinter
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import kotlin.time.ExperimentalTime

@ExperimentalTime
class TreeSearchingTest {

    private val input = Sudoco.Builder().withEntries(listOf(0,0,3,0,2,0,6,0,0,9,0,0,3,0,5,0,0,1,0,0,1,8,0,6,4,0,0,0,0,8,1,0,2,9,0,0,7,0,0,0,0,0,0,0,8,0,0,6,7,0,8,2,0,0,0,0,2,6,0,9,5,0,0,8,0,0,2,0,3,0,0,9,0,0,5,0,1,0,3,0,0)).build()
    private val easy = Sudoco.Builder().withEntries(listOf(0,0,0,8,1,5,3,0,4,5,0,3,0,0,7,6,1,0,1,4,8,0,9,0,0,0,2,0,0,0,0,0,1,4,7,3,9,6,0,3,7,0,0,0,5,4,3,7,5,0,8,0,6,0,0,5,9,1,8,0,0,4,0,6,7,0,4,0,9,5,0,0,0,0,4,0,5,0,9,3,6)).build()
    private val medium = Sudoco.Builder().withEntries(listOf(0,0,3,2,0,6,8,0,0,0,5,1,0,0,0,7,2,0,0,0,0,8,7,0,0,4,5,0,6,0,0,0,0,9,0,3,0,0,0,0,0,0,0,5,0,0,4,0,0,1,0,6,0,0,0,0,5,1,3,2,0,0,0,7,0,0,5,0,0,1,0,0,2,1,0,0,0,4,0,0,8)).build()
    private val hard = Sudoco.Builder().withEntries(listOf(0,0,4,0,0,0,5,1,0,0,0,0,0,0,9,8,0,0,0,0,7,8,3,0,6,0,4,8,0,0,0,0,0,0,6,0,0,0,0,0,9,0,0,8,0,0,0,6,0,0,4,0,0,0,0,0,0,0,1,0,9,0,0,4,2,0,7,0,0,0,0,0,5,6,0,0,0,8,0,4,0)).build()
    private val hard2 = Sudoco.Builder().withEntries(listOf(4,0,0,0,0,0,8,0,5,0,3,0,0,0,0,0,0,0,0,0,0,7,0,0,0,0,0,0,2,0,0,0,0,0,6,0,0,0,0,0,8,0,4,0,0,0,0,0,0,1,0,0,0,0,0,0,0,6,0,3,0,7,0,5,0,0,2,0,0,0,0,0,1,0,4,0,0,0,0,0,0)).build()
    private val expert = Sudoco.Builder().withEntries(listOf(0,9,0,0,0,0,3,0,8,0,0,3,5,9,0,6,0,0,0,0,0,0,0,0,7,0,0,0,0,2,8,0,0,0,0,5,0,1,0,3,0,0,0,0,0,4,0,0,0,0,6,0,8,1,5,0,0,0,2,7,0,0,0,0,0,9,0,0,0,0,0,0,0,0,0,0,1,0,0,6,0)).build()
    private val inkala2006 = Sudoco.Builder().withEntries(listOf(8,5,0,0,0,2,4,0,0,7,2,0,0,0,0,0,0,9,0,0,4,0,0,0,0,0,0,0,0,0,1,0,7,0,0,2,3,0,5,0,0,0,9,0,0,0,4,0,0,0,0,0,0,0,0,0,0,0,8,0,0,7,0,0,1,7,0,0,0,0,0,0,0,0,0,0,3,6,0,4,0)).build()
    private val inkala2010 = Sudoco.Builder().withEntries(listOf(0,0,5,3,0,0,0,0,0,8,0,0,0,0,0,0,2,0,0,7,0,0,1,0,5,0,0,4,0,0,0,0,5,3,0,0,0,1,0,0,7,0,0,0,6,0,0,3,2,0,0,0,8,0,0,6,0,5,0,0,0,0,9,0,0,4,0,0,0,0,3,0,0,0,0,0,0,9,7,0,0)).build()

    @Test
    fun `should have 81 squares`() {
        val subject = TreeSearching()
        assertEquals(81, subject.squares.size)
    }

    @Test
    fun `should have 27 unitlists`() {
        val subject = TreeSearching()
        assertEquals(27, subject.unitList.size)
    }

    @Test
    fun `should have 3 units for all squares`() {
        val subject = TreeSearching()
        subject.squares.forEach { s ->
            assertEquals(3, subject.units[s]!!.size)
        }
    }

    @Test
    fun `should have 20 peers for all squares`() {
        val subject = TreeSearching()
        subject.squares.forEach { s ->
            assertEquals(20, subject.peers[s]!!.size)
        }
    }

    @Test
    fun `should have correct units`() {
        val subject = TreeSearching()
        assertEquals(
            listOf(
                listOf("A2", "B2", "C2", "D2", "E2", "F2", "G2", "H2", "I2"),
                listOf("C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8", "C9"),
                listOf("A1", "A2", "A3", "B1", "B2", "B3", "C1", "C2", "C3")
            ), subject.units.get("C2")
        )
    }

    @Test
    fun `should have correct peers`() {
        val subject = TreeSearching()
        assertEquals(
            setOf(
                "A2", "B2", "D2", "E2", "F2", "G2", "H2", "I2",
                "C1", "C3", "C4", "C5", "C6", "C7", "C8", "C9",
                "A1", "A3", "B1", "B3"
            ), subject.peers.get("C2")
        )
    }

    @Test
    fun `should nike it with original sample (= just do it) for input`() {
        given(input).solve()
        assertTrue(input.blanks() == 0 && input.isValid())
    }
    @Test
    fun `should nike it (= just do it) for easy`() {
        given(easy).solve()
        assertTrue(easy.blanks() == 0 && easy.isValid())
    }

    @Test
    fun `should nike it (= just do it) for medium`() {
        given(medium).solve()
        assertTrue(medium.blanks() == 0 && medium.isValid())
    }

    @Test
    fun `should nike it (= just do it) for hard`() {
        given(hard).solve()
        assertTrue(hard.blanks() == 0 && hard.isValid())
    }

    @Test
    fun `should nike it (= just do it) for hard2`() {
        given(hard2).solve()
        assertTrue(hard2.blanks() == 0 && hard2.isValid())
    }

    @Test
    fun `should nike it (= just do it) for expert`() {
        given(expert).solve()
        assertTrue("blanks"){ expert.blanks() == 0 }
        assertTrue("valid"){ expert.isValid() }
    }

    @Test
    fun `should nike it (= just do it) for inkala2006`() {
        given(inkala2006).solve()
        assertTrue("blanks"){ inkala2006.blanks() == 0 }
        assertTrue("valid"){ inkala2006.isValid() }
    }

    @Test
    fun `should nike it (= just do it) for inkala2010`() {
        given(inkala2010).solve()
        assertTrue("blanks"){ inkala2010.blanks() == 0 }
        assertTrue("valid"){ inkala2010.isValid() }
    }

    private fun given(sudoco: Sudoco, algo: Algo = TreeSearching()) =
        Solver(
            sudoco,
            algo,
            AsciiPrinter()
        )
}