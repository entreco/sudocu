package nl.entreco.sudoco.shared

import nl.entreco.sudoco.shared.ArrayExtensions.nonetIndex
import kotlin.test.*

class ArrayExtensionsKtTest {

    @Test
    fun `should calculate correct nonet (0,0))`() {
        assertEquals(0, nonetIndex(0, 0))
    }

    @Test
    fun `should calculate correct nonet (0,1))`() {
        assertEquals(0, nonetIndex(0, 1))
    }

    @Test
    fun `should calculate correct nonet (2,5))`() {
        assertEquals(1, nonetIndex(2, 5))
    }

    @Test
    fun `should calculate correct nonet (2,6))`() {
        assertEquals(2, nonetIndex(2, 6))
    }

    @Test
    fun `should calculate correct nonet (3,1))`() {
        assertEquals(3, nonetIndex(3, 1))
    }

    @Test
    fun `should calculate correct nonet (5,5))`() {
        assertEquals(4, nonetIndex(5, 5))
    }

    @Test
    fun `should calculate correct nonet (3,6))`() {
        assertEquals(5, nonetIndex(3, 6))
    }

    @Test
    fun `should calculate correct nonet (6,1))`() {
        assertEquals(6, nonetIndex(6, 1))
    }

    @Test
    fun `should calculate correct nonet (8,5))`() {
        assertEquals(7, nonetIndex(8, 5))
    }

    @Test
    fun `should calculate correct nonet (6,6))`() {
        assertEquals(8, nonetIndex(6, 6))
    }
}