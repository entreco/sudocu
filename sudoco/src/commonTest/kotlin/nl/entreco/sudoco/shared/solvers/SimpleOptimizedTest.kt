package nl.entreco.sudoco.shared.solvers

import nl.entreco.sudoco.shared.Algo
import nl.entreco.sudoco.shared.Solver
import nl.entreco.sudoco.shared.Sudoco
import nl.entreco.sudoco.shared.printers.AsciiPrinter
import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue
import kotlin.time.ExperimentalTime

@ExperimentalTime
class SimpleOptimizedTest {

    private val empty = Sudoco.Builder().build()
    private val easy_1 = Sudoco.Builder().withEntries(listOf(0,0,0,8,1,5,3,0,4,5,0,3,0,0,7,6,1,0,1,4,8,0,9,0,0,0,2,0,0,0,0,0,1,4,7,3,9,6,0,3,7,0,0,0,5,4,3,7,5,0,8,0,6,0,0,5,9,1,8,0,0,4,0,6,7,0,4,0,9,5,0,0,0,0,4,0,5,0,9,3,6)).build()
    private val easy_2 = Sudoco.Builder().withEntries(listOf(0,0,0,6,7,0,0,1,3,0,1,0,0,4,0,8,0,2,0,0,0,0,3,0,0,9,0,3,0,8,0,1,5,0,0,6,4,0,0,7,6,0,0,5,8,0,0,1,9,0,0,0,0,7,0,0,0,0,9,0,2,0,0,8,3,2,1,5,7,0,0,9,0,9,0,2,0,4,7,3,5)).build()

    @Test
    fun `should not loop endlessly`() {
        givenSimple(empty).solve()
        assertFalse(empty.blanks() == 0)
    }

    @Test
    fun `should solve easy sudoco - 1`() {
        givenSimple(easy_1).solve()
        assertTrue(easy_1.blanks() == 0)
    }

    @Test
    fun `should solve easy sudoco - 2`() {
        givenSimple(easy_2).solve()
        assertTrue(easy_2.blanks() == 0)
    }

    private fun givenSimple(sudoco: Sudoco, algo: Algo = SimpleAlgo()) =
        Solver(
            sudoco,
            algo,
            AsciiPrinter()
        )
}