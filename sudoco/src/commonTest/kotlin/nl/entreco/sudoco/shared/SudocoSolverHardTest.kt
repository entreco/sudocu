package nl.entreco.sudoco.shared

import nl.entreco.sudoco.shared.printers.AsciiPrinter
import nl.entreco.sudoco.shared.solvers.TreeSearching
import kotlin.test.Test
import kotlin.test.assertTrue
import kotlin.time.ExperimentalTime

@ExperimentalTime
class SudocoSolverHardTest {

    private val hard1 = Sudoco.Builder().withEntries(listOf(9,0,0,0,0,0,0,0,5,0,3,0,2,0,0,0,0,0,0,1,5,3,4,0,0,7,0,6,0,0,1,3,0,0,4,9,7,0,0,0,0,0,6,0,0,2,0,0,0,0,4,0,0,0,0,0,0,0,0,0,1,0,3,0,0,0,9,1,0,0,0,0,0,7,9,0,8,0,0,0,0)).build()
    private val hard2 = Sudoco.Builder().withEntries(listOf(0,0,0,0,0,0,9,4,0,6,0,0,0,0,0,2,7,0,8,2,0,0,4,9,6,0,0,0,7,4,0,0,0,0,0,0,1,0,0,7,6,0,0,0,0,0,6,2,0,0,5,0,8,0,0,0,0,0,5,7,0,2,3,0,0,0,0,0,0,0,0,0,7,5,3,2,0,4,0,0,0)).build()
    private val hard3 = Sudoco.Builder().withEntries(listOf(0,0,4,0,0,0,5,1,0,0,0,0,0,0,9,8,0,0,0,0,7,8,3,0,6,0,4,8,0,0,0,0,0,0,6,0,0,0,0,0,9,0,0,8,0,0,0,6,0,0,4,0,0,0,0,0,0,0,1,0,9,0,0,4,2,0,7,0,0,0,0,0,5,6,0,0,0,8,0,4,0)).build()

    @Test
    fun `should solve hard sudoco - 1`() {
        given(hard1).solve()
        assertTrue(hard1.blanks() == 0 && hard1.isValid())
    }

    @Test
    fun `should solve hard sudoco - 2`() {
        given(hard2).solve()
        assertTrue(hard2.blanks() == 0 && hard2.isValid())
    }

    @Test
    fun `should solve hard sudoco - 3`() {
        given(hard3).solve()
        assertTrue(hard3.blanks() == 0 && hard3.isValid())
    }

    @Test
    fun `should solve hard sudoco - 4`() {
        given(hard3).solve()
        assertTrue(hard3.blanks() == 0 && hard3.isValid())
    }

    private fun given(sudoco: Sudoco, algo: Algo = TreeSearching()) = Solver(
        sudoco,
        algo,
        AsciiPrinter()
    )
}