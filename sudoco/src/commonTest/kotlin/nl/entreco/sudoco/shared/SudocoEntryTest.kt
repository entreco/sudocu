package nl.entreco.sudoco.shared

import kotlin.test.Test
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class SudocoEntryTest {

    @Test
    fun `should be valid initially`() {
        assertTrue(Sudoco.Builder().build().isValid())
    }

    @Test
    fun `should allow rows of 1 through 9`() {
        assertTrue(row(listOf(1, 2, 3, 4, 5, 6, 7, 8, 9)))
    }

    @Test
    fun `should not allow rows with duplicates (less than 9)`() {
        assertFalse(row(listOf(1, 1)))
    }

    @Test
    fun `should not allow rows with duplicates (more than 9)`() {
        assertFalse(row(listOf(1, 2, 3, 4, 5, 6, 7, 8, 9, 1)))
    }

    @Test
    fun `should allow empty places`() {
        assertTrue(row(listOf(0, 2, 3, 4, 5, 6, 7, 8, 9)))
    }

    @Test
    fun `should allow entires blank row`() {
        assertTrue(row(listOf(0, 0, 0, 0, 0, 0, 0, 0, 0)))
    }

    private fun row(entries: List<Int>): Boolean {
        return Algo(2).check(entries)
    }

    class Algo(private val which: Int = 1) {
        fun check(entries: List<Int>): Boolean = when (which) {
            1 -> sortedCompare(entries)
            2 -> setCount(entries)
            else -> setCount(entries)
        }

        private fun sortedCompare(entries: List<Int>): Boolean {
            val expected = 1..9
            return entries.sorted() == expected.toList()
        }

        private fun setCount(entries: List<Int>): Boolean {
            val set = mutableSetOf<Int>()
            var blankCounter = -1
            entries.asSequence().forEachIndexed { index, number ->
                if (number == 0) {
                    blankCounter++
                } else {
                    set.add(number)
                }

                if (blankCounter + set.size != index) return false
            }
            return true
        }
    }
}