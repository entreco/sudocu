package nl.entreco.sudoco.shared

import nl.entreco.sudoco.shared.printers.AsciiPrinter
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

class SudocoCreatorTest {

    @Test
    fun `should set value at correct row and column (7,6)=5`() {
        val s =  Sudoco.Builder()
            .withEntries(listOf(8, 5, 0, 0, 0, 2, 4, 0, 0, 7, 2, 0, 0, 0, 0, 0, 0, 9, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 7, 0, 0, 2, 3, 0, 5, 0, 0, 0, 9, 0, 0 ,0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 7, 0, 0, 1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 6, 0, 4, 0))
            .build()
        assertEquals("""
                ╔═══╤═══╤═══╦═══╤═══╤═══╦═══╤═══╤═══╗
                ║ 8 │ 5 │   ║   │   │ 2 ║ 4 │   │   ║
                ╟───┼───┼───╫───┼───┼───╫───┼───┼───╢
                ║ 7 │ 2 │   ║   │   │   ║   │   │ 9 ║
                ╟───┼───┼───╫───┼───┼───╫───┼───┼───╢
                ║   │   │ 4 ║   │   │   ║   │   │   ║
                ╠═══╪═══╪═══╬═══╪═══╪═══╬═══╪═══╪═══╣
                ║   │   │   ║ 1 │   │ 7 ║   │   │ 2 ║
                ╟───┼───┼───╫───┼───┼───╫───┼───┼───╢
                ║ 3 │   │ 5 ║   │   │   ║ 9 │   │   ║
                ╟───┼───┼───╫───┼───┼───╫───┼───┼───╢
                ║   │ 4 │   ║   │   │   ║   │   │   ║
                ╠═══╪═══╪═══╬═══╪═══╪═══╬═══╪═══╪═══╣
                ║   │   │   ║   │ 8 │   ║   │ 7 │   ║
                ╟───┼───┼───╫───┼───┼───╫───┼───┼───╢
                ║   │ 1 │ 7 ║   │   │   ║   │   │   ║
                ╟───┼───┼───╫───┼───┼───╫───┼───┼───╢
                ║   │   │   ║   │ 3 │ 6 ║   │ 4 │   ║
                ╚═══╧═══╧═══╩═══╧═══╧═══╩═══╧═══╧═══╝
        """.trimIndent(), AsciiPrinter().print(s))
    }

    @Test
    fun `should check if valid - 1`() {
        val s =  Sudoco.Builder()
            .withEntries(listOf(8, 5, 0, 0, 0, 2, 4, 0, 0, 7, 2, 0, 0, 0, 0, 0, 0, 9, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 7, 0, 0, 2, 3, 0, 5, 0, 0, 0, 9, 0, 0 ,0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 7, 0, 0, 1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 6, 0, 4, 0))
            .build()
        assertTrue(s.isValid())
    }

    @Test
    fun `should check if valid - 2`() {
        val s =  Sudoco.Builder()
            .withEntries(listOf(8, 5, 1, 0, 0, 2, 4, 0, 0, 7, 2, 0, 0, 0, 0, 0, 0, 9, 1, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 7, 0, 0, 2, 3, 0, 5, 0, 0, 0, 9, 0, 0 ,0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 8, 0, 0, 7, 0, 0, 1, 7, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 6, 0, 4, 0))
            .build()
        assertFalse(s.isValid())
    }

    @Test
    fun `should check if valid 2x2`() {
        val s =  Sudoco.Builder(2)
            .withEntries(listOf(1,2,3,4))
            .build()
        assertTrue(s.isValid())
    }

    @Test
    fun `should check if valid 3x3`() {
        val s =  Sudoco.Builder(3)
            .withEntries(listOf(1,2,3,4,5,6,7,8,9))
            .build()
        assertTrue(s.isValid())
    }

    @Test
    fun `should detect invalid rows`() {
        val s =  Sudoco.Builder(2)
            .withEntries(listOf(1,1,2,3))
            .build()
        assertFalse(s.isValid())
    }

    @Test
    fun `should detect invalid columns`() {
        val s =  Sudoco.Builder(2)
            .withEntries(listOf(1,2,1,3))
            .build()
        assertFalse(s.isValid())
    }

    @Test
    fun `should detect invalid blocks 1`() {
        val s =  Sudoco.Builder(3)
            .withEntries(listOf(1,2,3,
                                2,3,1,
                                3,1,2))
            .build()
        assertFalse(s.isValid())
    }

    @Test
    fun `should detect invalid blocks 2`() {
        val s =  Sudoco.Builder(3)
            .withEntries(listOf(1,2,3,
                                4,5,6,
                                7,8,9))
            .build()
        assertTrue(s.isValid())
    }
}