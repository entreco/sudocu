package nl.entreco.sudoco.shared

import nl.entreco.sudoco.shared.printers.AsciiPrinter
import nl.entreco.sudoco.shared.solvers.SimpleAlgo
import kotlin.test.Test
import kotlin.test.assertTrue
import kotlin.time.ExperimentalTime

@ExperimentalTime
class SudocoSolverMediumTest {

    private val medium_1 = Sudoco.Builder().withEntries(listOf(4,0,0,0,0,8,0,0,2,0,6,0,0,2,0,8,0,5,0,0,0,0,6,0,0,3,0,3,9,6,0,0,0,1,2,8,0,0,7,0,0,2,0,4,0,2,0,4,0,0,0,5,7,0,0,7,2,0,8,4,0,0,0,0,3,0,0,0,0,0,0,9,5,4,9,6,1,0,0,8,0)).build()
    private val medium_2 = Sudoco.Builder().withEntries(listOf(0,0,3,2,0,6,8,0,0,0,5,1,0,0,0,7,2,0,0,0,0,8,7,0,0,4,5,0,6,0,0,0,0,9,0,3,0,0,0,0,0,0,0,5,0,0,4,0,0,1,0,6,0,0,0,0,5,1,3,2,0,0,0,7,0,0,5,0,0,1,0,0,2,1,0,0,0,4,0,0,8)).build()
    private val medium_3 = Sudoco.Builder().withEntries(listOf(0,7,2,5,0,0,0,0,4,0,0,0,8,0,0,6,0,0,8,3,0,0,0,0,2,5,0,1,0,0,0,9,6,8,0,0,0,8,4,0,0,0,0,9,6,0,0,6,0,1,8,7,4,0,4,0,0,9,0,0,0,0,0,0,0,3,0,0,5,0,0,0,0,2,0,3,7,0,0,0,0)).build()

    @Test
    fun `should solve medium sudoco - 1`() {
        givenSimple(medium_1).solve()
        assertTrue(medium_1.blanks() == 0 && medium_1.isValid())
    }

    @Test
    fun `should solve medium sudoco - 2`() {
        givenSimple(medium_2).solve()
        assertTrue(medium_2.blanks() == 0 && medium_2.isValid())
    }

    private fun givenSimple(sudoco: Sudoco, algo: Algo = SimpleAlgo()) = Solver(
        sudoco,
        algo,
        AsciiPrinter()
    )
}